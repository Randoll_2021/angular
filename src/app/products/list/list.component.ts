import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ProductListComponent implements OnInit {
  products: any[] = new Array<any>();
  constructor(private angularFirestore: AngularFirestore) {}

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    var product: { [k: string]: any } = {};
    this.products.length = 0;
    this.angularFirestore
      .collection('product_collection')
      .get()
      .subscribe((res) => {
        res.docs.forEach((item) => {
          product = item.data();
          product.id = item.id;
          product.ref = item.ref;
          this.products.push(product);
        });
      });
  }

  deleteProduct(productId: any) {
    if (confirm('¿Desea eliminar este producto?')) {
      this.angularFirestore
        .doc('product_collection' + '/' + productId)
        .delete()
        .then((res) => {
          alert('Producto eliminado!');
          this.getProducts();
        });
    }
  }
}
