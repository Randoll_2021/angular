import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.css'],
})
export class ProductCreateEditComponent implements OnInit {
  productForm: FormGroup;
  submitted = false;
  productId: string;
  title: string;

  constructor(
    private db: AngularFirestore,
    private formBuilder: FormBuilder,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      id: 0,
      name: ['', Validators.required],
      price: [0, Validators.required],
      cost: [0, Validators.required],
      quantity: [0, Validators.required],
    });

    this.productId = '0';
    this.title = 'Crear';

    this.activeRoute.params.subscribe((params) => {
      this.productId = params['id'];
      if (this.productId != undefined) {
        this.title = 'Editar';

        this.db
          .doc<any>('product_collection' + '/' + this.productId)
          .valueChanges()
          .subscribe((res) => {
            this.productForm.setValue({
              id: this.productId,
              name: res.name,
              price: res.price,
              cost: res.cost,
              quantity: res.quantity,
            });
          });
      }
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.productForm.invalid) {
      return;
    }

    if (this.productId == undefined) {
      this.db
        .collection('product_collection')
        .add(this.productForm.value)
        .then((res) => {
          alert('registros insertados.');
          this.backToList();
        });
    } else {
      this.db
        .doc('product_collection/' + this.productId)
        .update(this.productForm.value)
        .then((res) => {
          alert('registros actualizados.');
          this.backToList();
        });
    }
  }

  backToList() {
    this.productForm.reset();
    this.router.navigate(['../list']);
  }
}
