import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductCreateEditComponent } from './products/create-edit/create-edit.component';
import { ProductListComponent } from './products/list/list.component';

const routes: Routes = [
  {
    path: '',
    component :HomeComponent
  },
  {
    path: 'list',
    component: ProductListComponent
  },
  {
    path: 'create',
    component: ProductCreateEditComponent
  },
  {
    path: 'edit/:id',
    component: ProductCreateEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
